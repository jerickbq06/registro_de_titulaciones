import { BrowserRouter, Route, Routes } from "react-router-dom";
import { ReservasProvider } from "./Screens/ReservasContext";
import "./App.css"; 
import Footer from "./Screens/Footer";
import Principal from "./Screens/Principal";
import Login from "./Screens/Login";
import Registro from "./Screens/Registro";
import Opciones from "./Screens/Opciones";
import Datos from "./Screens/Datos";
import Asignar from "./Screens/Asignar";
function App() {
  return (
    <div className="App">
      <ReservasProvider>
      <BrowserRouter>
      <Routes>
        <Route path="/" element={<Principal />} />
        <Route path="/login" element={<Login />} />
        <Route path="/registro" element={<Registro />} />
        <Route path="/opciones" element={<Opciones />} />
        <Route path="/datos" element={<Datos />} />
        <Route path="/asignar" element={<Asignar/>} />
      </Routes>
      <Footer />
      
      </BrowserRouter>
      </ReservasProvider>


      

  

    </div>
  );
}

export default App;
