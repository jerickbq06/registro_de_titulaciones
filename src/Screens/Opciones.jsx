import React from "react";
import { Link } from "react-router-dom";
import "../Components/Opciones.css";

function Opciones() {
  return (
    <div>
      <div className="navbar">
        <div className="navbar-container">
          <Link to="/">
            <img className="logo" src="/log.png" alt="logo" width={180} />
          </Link>
          <ul className="nav">
            <li className="nav-item">
              <Link className="nav-link button" to="/">
                Cerrar Sesion
              </Link>
            </li>
          </ul>
        </div>
      </div>
      <div className="container">
        <h1>Control de Trabajos de Titulacion</h1>
        <div className="box">
          <h2>Asignar Trabajos</h2>
          <p>Escoger Trabajos de titulacion disponibles</p>
          <Link to="/asignar">
            <button className="button">Asignar</button>
          </Link>
        </div>

        <div className="box">
          <h2>Gestionar Trabajos</h2>
          <p>Trabajos Registrados</p>
          <Link to="/datos">
            <button className="button">Gestionar</button>
          </Link>
        </div>
      </div>
    </div>
  );
}

export default Opciones;
