import React from "react";
import "../Components/Principal.css";
import { Link } from "react-router-dom";

function Principal() {
  return (
    <div>
      <div className="navbar">
        <div className="navbar-container">
          <Link to="/">
            <img className="logo" src="/log.png" alt="logo" width={180} />
          </Link>
          <ul className="nav">
            <li className="nav-item">
              <Link className="nav-link button" to="/registro">
                Registrarse
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link button" to="/login">
                Iniciar Sesión
              </Link>
            </li>
          </ul>
        </div>
      </div>
      <div className="center-box">
        <h2>Registro y Control de Trabajos de Titulacion</h2>
        <p>
          Bienvenido a nuestro centro de gestion y control de Trabajos de
          Tiulacion que se realizan en la Universidad Laica Eloy Alfaro
        </p>
      </div>
    </div>
  );
}

export default Principal;
