import React, { useState } from "react";
import { useReservas } from "./ReservasContext";
import { Link } from "react-router-dom";

const OtroFormulario = () => {
  const { agregarReserva } = useReservas();
  const [formData, setFormData] = useState({
    fechaEntrada: "",
    nombre: "",
    proyecto: "",
    carrera: "",
    correo: "",
    telefono: "",
  });

  const handleSubmit = (e) => {
    e.preventDefault();

    // Validación de campos llenos
    const camposLlenos = Object.values(formData).every((value) => value !== "");

    if (!camposLlenos) {
      alert("Todos los campos deben estar llenos.");
      return;
    }

    // Guardar datos en el contexto de reservas
    agregarReserva(formData);

    // Limpiar el formulario después de guardar la reserva
    setFormData({
      fechaEntrada: "",
      nombre: "",
      proyecto: "",
      carrera: "",
      correo: "",
      telefono: "",
    });

    alert("Datos guardados con éxito!");
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  return (
    <div>
      <div className="navbar">
        <div className="navbar-container">
          <Link to="/opciones">
            <img className="logo" src="/log.png" alt="logo" width={180} />
          </Link>
          <ul className="nav">
            <li className="nav-item">
              <Link className="nav-link button" to="/">
                Cerrar Sesion
              </Link>
            </li>
          </ul>
        </div>
      </div>
    <div className="reservas-contenedor">
      <form onSubmit={handleSubmit}>
        <h2>Registrar Proyectos</h2>
        <div className="formulario-reservar">
          <label htmlFor="fecha-entrada">Fecha de ingreso:</label>
          <input
            type="date"
            id="fecha-entrada"
            name="fechaEntrada"
            value={formData.fechaEntrada}
            onChange={handleChange}
            required
          />
        </div>
        <div className="formulario-reservar">
          <label htmlFor="nombre">Nombre completo:</label>
          <input
            type="text"
            id="nombre"
            name="nombre"
            value={formData.nombre}
            onChange={handleChange}
            required
          />
        </div>

        <div className="formulario-reservar">
          <label htmlFor="proyecto">Nombre del Proyecto:</label>
          <input
            type="text"
            id="proyecto"
            name="proyecto"
            value={formData.proyecto}
            onChange={handleChange}
            required
          />
        </div>

        <div className="formulario-reservar">
          <label htmlFor="carrera">Carrera:</label>
          <input
            type="text"
            id="carrera"
            name="carrera"
            value={formData.carrera}
            onChange={handleChange}
            required
          />
        </div>

        <div className="formulario-reservar">
          <label htmlFor="correo">Correo electrónico:</label>
          <input
            type="email"
            id="correo"
            name="correo"
            value={formData.correo}
            onChange={handleChange}
            required
          />
        </div>

        <div className="formulario-reservar">
          <label htmlFor="telefono">Teléfono:</label>
          <input
            type="tel"
            id="telefono"
            name="telefono"
            value={formData.telefono}
            onChange={handleChange}
            required
            pattern="[0-9]+"
          />
        </div>
        <button type="submit">Guardar</button>
        <Link to="/datos">
          <button class = "boton-ver-registros">Ver Registros</button>
        </Link>
      </form>
    </div>
  </div>
  );
};

export default OtroFormulario;
