// ReservasContext.js
import React, { createContext, useContext, useState } from 'react';

const ReservasContext = createContext();

export const useReservas = () => {
  return useContext(ReservasContext);
};

export const ReservasProvider = ({ children }) => {
  const [reservas, setReservas] = useState([]);

  const agregarReserva = (nuevaReserva) => {
    setReservas([...reservas, nuevaReserva]);
  };

  return (
    <ReservasContext.Provider value={{ reservas, agregarReserva }}>
      {children}
    </ReservasContext.Provider>
  );
};
