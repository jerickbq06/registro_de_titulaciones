import React from "react";
import "../Components/Registro.css";
import { Link } from "react-router-dom";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

const Registro = () => {
  const navigate = useNavigate();

  const [formData, setFormData] = useState({
    username: "",
    email: "",
    password: "",
    confirmPassword: "",
    acceptTerms: false,
  });

  const handleChange = (e) => {
    const { name, value, type, checked } = e.target;

    setFormData((prevData) => ({
      ...prevData,
      [name]: type === "checkbox" ? checked : value,
    }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (formData.password !== formData.confirmPassword) {
      alert("Las contraseñas no coinciden");
      return;
    }

    // Guarda la información del usuario en el estado local o envía al servidor
    localStorage.setItem("user", JSON.stringify(formData));

    // Redirige a la página de inicio de sesión
    navigate("/login");
  };
  return (
    <div>
      <Link to="/">
        <img className="logo" src="/log.png" alt="logo" width={180} />
      </Link>
      <form onSubmit={handleSubmit}>
        <h2>REGISTRO</h2>
        <div className="separador">
          <label htmlFor="username">Nombre de usuario:</label>
          <input
            type="text"
            id="username"
            name="username"
            placeholder="Ingresa tu nombre de usuario"
            value={formData.username}
            onChange={handleChange}
            required
          />
        </div>
        <div className="separador">
          <label htmlFor="email">Correo electrónico:</label>
          <input
            type="email"
            id="email"
            name="email"
            placeholder="Ingresa tu correo electrónico"
            value={formData.email}
            onChange={handleChange}
            required
          />
        </div>
        <div className="separador">
          <label htmlFor="password">Contraseña:</label>
          <input
            type="password"
            id="password"
            name="password"
            placeholder="Ingresa tu contraseña"
            value={formData.password}
            onChange={handleChange}
            required
          />
        </div>
        <div className="separador">
          <label htmlFor="confirm-password">Confirmar contraseña:</label>
          <input
            type="password"
            id="confirm-password"
            name="confirmPassword"
            placeholder="Confirma tu contraseña"
            value={formData.confirmPassword}
            onChange={handleChange}
            required
          />
        </div>
        <div className="input-container">
          {/* Otros campos de entrada */}
          <div className="label-container">
            <input
              type="checkbox"
              id="accept-terms"
              name="acceptTerms"
              checked={formData.acceptTerms}
              onChange={handleChange}
              required
            />
            <label htmlFor="accept-terms">
              Acepto los términos y condiciones
            </label>
          </div>
        </div>
        <p>
          ¿Ya tienes una cuenta? <Link to={"/login"}>Inicia sesión aquí</Link>
        </p>
        <button type="submit">Registrarse</button>
      </form>
    </div>
  );
};

export default Registro;
