// TablaReservas.js
import React from "react";
import "../Components/Datos.css";
import { Link } from "react-router-dom";
import { useReservas } from "./ReservasContext";

const Datos = () => {
  const { reservas } = useReservas();

  return (
    <div>
      <div className="navbar">
        <div className="navbar-container">
          <Link to="/opciones">
            <img className="logo" src="/log.png" alt="logo" width={180} />
          </Link>
          <ul className="nav">
            <li className="nav-item">
              <Link className="nav-link button" to="/">
                Cerrar Sesion
              </Link>
            </li>
          </ul>
        </div>
        <div>
          <div className="tabla-container">
            <h2>Reservas</h2>
            <table>
              <thead>
                <tr>
                  <th>Fecha de Ingreso</th>
                  <th>Nombre</th>
                  <th>Proyecto</th>
                  <th>Carrera</th>
                  <th>Correo</th>
                  <th>Teléfono</th>
                </tr>
              </thead>
              <tbody>
                {reservas.map((reserva, index) => (
                  <tr key={index}>
                    <td>{reserva.fechaEntrada}</td>
                    <td>{reserva.nombre}</td>
                    <td>{reserva.proyecto}</td>
                    <td>{reserva.carrera}</td>
                    <td>{reserva.correo}</td>
                    <td>{reserva.telefono}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Datos;
