import { Link } from "react-router-dom";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

const Login = () => {
  const navigate = useNavigate();

  const [loginData, setLoginData] = useState({
    username: "",
    password: "",
  });

  const handleChange = (e) => {
    const { name, value } = e.target;

    setLoginData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleLogin = (e) => {
    e.preventDefault();

    // Obtiene la información del usuario registrado desde el estado local
    const registeredUser = JSON.parse(localStorage.getItem("user"));

    if (!registeredUser) {
      alert("No hay usuarios registrados. Regístrate primero.");
      return;
    }

    // Realiza la validación del inicio de sesión
    if (
      loginData.username === registeredUser.username &&
      loginData.password === registeredUser.password
    ) {
      // Inicio de sesión exitoso, puedes redirigir o realizar acciones adicionales
      alert("Inicio de sesión exitoso");
      navigate("/Opciones");
    } else {
      alert("Usuario o contraseña incorrectos");
    }
  };

  return (
    <div>
          <Link to="/">
            <img className="logo" src="/log.png" alt="logo" width={180} />
          </Link>

      <form onSubmit={handleLogin}>
        <h2>INICIO DE SESIÓN</h2>
        <div className="separador">
          <label htmlFor="username">Nombre de usuario:</label>
          <input
            type="text"
            id="username"
            name="username"
            placeholder="Ingresa tu nombre de usuario"
            value={loginData.username}
            onChange={handleChange}
            required
          />
        </div>
        <div className="separador">
          <label htmlFor="password">Contraseña:</label>
          <input
            type="password"
            id="password"
            name="password"
            placeholder="Ingresa tu contraseña"
            value={loginData.password}
            onChange={handleChange}
            required
          />
        </div>
        <p>
        ¿Aun no tienes una cuenta ? <Link to={"/registro"}>Registraste Aqui</Link>
        </p>
        <button type="submit">Iniciar sesión</button>
      </form>
    </div>
  );
};

export default Login;
